package com.example.demopojo2.services;

import com.example.demopojo2.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    boolean save(User user);

    User getOne(Integer id);

    boolean update(User user);
}
