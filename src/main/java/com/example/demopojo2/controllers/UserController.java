package com.example.demopojo2.controllers;

import com.example.demopojo2.models.User;
import com.example.demopojo2.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/all")
        public String getAll(ModelMap modelMap){

        List<User> users = userService.getAll();
        modelMap.addAttribute("userList", users);


        return "index";
    }


    @GetMapping("/users/one/{user_id}")
    public String getAll(@PathVariable("user_id") Integer id, ModelMap modelMap){
        User user = userService.getOne(id);
        modelMap.addAttribute("user", user);
        return "view_user";
    }

    @GetMapping("/users/add")
    public String showAddUserForm(Model model){

        model.addAttribute("user", new User());

        return "add_new";
    }

    @PostMapping("/users/add/submit")
    public String submitUser(User user){
        System.out.println(user);
        this.userService.save(user);
        return "redirect:/users/all";
    }

    @GetMapping("/users/update/{id}")
    public String showUpdateUserForm(@PathVariable("id") Integer id, Model model){

        User user = this.userService.getOne(id);
        model.addAttribute("user", user);
        return "update-user";
    }

    @GetMapping("/users/update/submit")
    public String updateUserSubmit(User user){

        this.userService.update(user);

        return "redirect:/users/all";
    }

}
