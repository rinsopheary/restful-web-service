package com.example.demopojo2.restcontroller;

import com.example.demopojo2.models.User;
import com.example.demopojo2.services.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {

    private UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<User> getUser(){
        return userService.getAll();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public boolean addUser(@RequestBody User user){
        return this.userService.save(user);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public boolean update(@RequestBody User user){
        return this.userService.update(user);
    }
}
