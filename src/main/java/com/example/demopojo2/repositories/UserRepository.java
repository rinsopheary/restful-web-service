package com.example.demopojo2.repositories;

import com.example.demopojo2.models.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {

    List<User> userList = new ArrayList<>();

    {
        userList.add(new User(1,"Dara", "Male"));
        userList.add(new User(2,"Nich", "Male"));
        userList.add(new User(3,"Makara", "Male"));
        userList.add(new User(4,"Nita", "Female"));
    }

    public List<User> getAll(){
        return this.userList;
    }

    public User getOne(Integer id){

        for(User u: userList){
            Integer user_id = u.getId();
            if(user_id.equals(id)){
                return u;
            }
        }

        return null;
    }

    public boolean save(User user){
        return userList.add(user);
    }

    public boolean update(User user){
        for(int i=0;i<userList.size();i++){
            if(userList.get(i).getId().equals(user.getId())){
                userList.set(i,user);
                return true;
            }
        }
        return false;
    }

}
