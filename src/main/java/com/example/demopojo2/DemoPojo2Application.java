package com.example.demopojo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPojo2Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoPojo2Application.class, args);
    }

}
